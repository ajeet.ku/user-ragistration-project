package org.neosoft.application.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="app_user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	private String surName;
	
	private String firstName;
	
	private String lastName;
	
	private Date dateOfBirth;
	
	private Date joiningDate;
	
	private String address;
	
	private String state;
	
	private String city;
	
	private String pincode;
	
	private Integer isDiscard;

	
	private Integer phoneNumber;
	
	private String emailId;

	
	
	
	
	

}
