package org.neosoft.application.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseResult {
	
	private List<String> errors;
	private String message;
	private int statusCode;
	//private Object data;
	public ResponseResult(List<String> errors, String message, int statusCode) {
		super();
		this.errors = errors;
		this.message = message;
		this.statusCode = statusCode;
		//this.data=data;
	}
	
	
	

}
