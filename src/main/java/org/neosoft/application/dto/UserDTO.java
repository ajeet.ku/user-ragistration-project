package org.neosoft.application.dto;



import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
	
	private Integer id;
	@NotEmpty(message = "SurName not be empty")
	private String surName;
	@NotEmpty(message = "First Name not be empty")
	private String firstName;
	
	private String lastName;
	
	@NotEmpty(message = "Date Of Birth not be empty")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateOfBirth;
	@NotEmpty(message = "Date of joining not be empty")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String joiningDate;
	
	private String address;
	
	private String state;
	
	private String city;
	
	private String pincode;
	
	private Integer isDiscard;
	
	
	
	@NotNull(message = "Phone Number not be empty")
	private Integer phoneNumber;
	
	@NotEmpty(message = "Email Id Number not be empty")
	private String emailId;
	


}

