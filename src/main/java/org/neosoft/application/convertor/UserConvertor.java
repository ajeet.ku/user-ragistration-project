package org.neosoft.application.convertor;

import java.util.Date;

import org.neosoft.application.dto.UserDTO;
import org.neosoft.application.entity.User;
import org.neosoft.application.util.DateTimeUtil;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class UserConvertor {
	
	public User dtoToEntity(UserDTO dto)
	{
		
		return User.builder().surName(dto.getSurName()).firstName(dto.getFirstName()).lastName(dto.getLastName())
		.dateOfBirth(DateTimeUtil.strToDate(dto.getDateOfBirth())).joiningDate(DateTimeUtil.strToDate(dto.getJoiningDate())).address(dto.getAddress())
		.state(dto.getState()).city(dto.getCity()).pincode(dto.getPincode()).phoneNumber(dto.getPhoneNumber()).emailId(dto.getEmailId())
		.isDiscard(0).build();
		
	}
	
	public User  dtoToUpdateEntity(User user,UserDTO dto)
	{
		user.setId(dto.getId());
		user.setAddress(dto.getAddress());
		user.setCity(dto.getCity());
		user.setDateOfBirth(DateTimeUtil.strToDate(dto.getDateOfBirth()));
		user.setEmailId(dto.getEmailId());
		user.setFirstName(dto.getFirstName());
		user.setIsDiscard(dto.getIsDiscard());
		user.setJoiningDate(DateTimeUtil.strToDate(dto.getJoiningDate()));
		user.setLastName(dto.getLastName());
		user.setPhoneNumber(dto.getPhoneNumber());
		user.setPincode(dto.getPincode());
		user.setState(dto.getState());
		user.setSurName(dto.getSurName());
		
		return user;
		
	}
	
	public UserDTO entityToDTO(User user)
	{
		return UserDTO.builder().id(user.getId()).surName(user.getSurName()).firstName(user.getFirstName()).lastName(user.getLastName())
				.dateOfBirth(DateTimeUtil.dateToString(user.getDateOfBirth())).joiningDate(DateTimeUtil.dateToString(user.getJoiningDate())).address(user.getAddress())
				.state(user.getState()).city(user.getCity()).pincode(user.getPincode()).phoneNumber(user.getPhoneNumber()).emailId(user.getEmailId())
				.isDiscard(user.getIsDiscard()).build();
		
	}

}
