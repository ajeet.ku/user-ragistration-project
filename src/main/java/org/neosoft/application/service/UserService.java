package org.neosoft.application.service;

import java.util.List;

import org.neosoft.application.entity.User;
import org.springframework.stereotype.Service;

@Service("userService")
public interface UserService {
	
	
 User save(User user);
 
 User getUserById(Integer id);
 
 List<User> getAllUser();
	
 void deleteById(Integer id);
 
 //public List<User> findUserSortingByField(String dateOfBirth,String  joiningDate);
 
 

}
