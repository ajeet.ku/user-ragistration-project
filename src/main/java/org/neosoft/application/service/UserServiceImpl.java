package org.neosoft.application.service;

import java.util.List;

import org.neosoft.application.entity.User;
import org.neosoft.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service("userService")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public User getUserById(Integer id) {
		return userRepository.findById(id).orElse(null);
	}

	@Override
	public List<User> getAllUser() {
		List<User> userList=userRepository.findAll();
		
		return userList;
	}

	@Override
	public void deleteById(Integer id) {
		userRepository.deleteById(id);
	}

//	@Override
//	public List<User> findUserSortingByField(String dateOfBirth,String  joiningDate) {
//		
//		return userRepository.findAll(Sort.by(dateOfBirth).and(Sort.by(joiningDate)));
//	}

}
