package org.neosoft.application.exceptionhandler;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Component
public class ExceptionHandler extends ResponseEntityExceptionHandler{

}
